module.exports = function(str) {
    let separator = /[A-Za-z0-9А-Яа-я]+/,
        masDividers = str.split(separator);
    masDividers = masDividers.filter(element => element);
    masDividers.sort((a, b) => a.toString().length - b.toString().length);
    let elem = masDividers.filter(element => element ==  masDividers[masDividers.length - 1]).length > 1 ?
        masDividers[0] : masDividers[masDividers.length - 1],
        mod = masDividers[0] == elem? masDividers[masDividers.length - 1] : masDividers[0];
    return {
        mod : mod,
        elem : elem
    }

}